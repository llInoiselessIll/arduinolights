#include <FastLED.h>

#define LED_PIN_1   5
#define LED_PIN_2   9
#define LED_PIN_3   10
#define NUM_LEDS_1  1
#define NUM_LEDS_2  150

bool gReverseDirection = false;

int current_effect = 0;
unsigned long last_change = 0;
#define last_change_wait_short 100

CRGBPalette16 currentPalette;
TBlendType    currentBlending;

extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;

CRGB leds_1[NUM_LEDS_1];
CRGB leds[NUM_LEDS_2];

void setup() 
{
  FastLED.addLeds<WS2812B, LED_PIN_1>(leds_1, NUM_LEDS_1);
  FastLED.addLeds<NEOPIXEL, LED_PIN_2>(leds, NUM_LEDS_2);
  FastLED.addLeds<NEOPIXEL, LED_PIN_3>(leds, NUM_LEDS_2);
  pinMode(7, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(7), change_effect, LOW);
  
    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;
}

void fadeall() { for(int i = 0; i < NUM_LEDS_2; i++) { leds[i].nscale8(250); } }

void loop()
{
     switch (current_effect)
    {
      case 0:
        white();
        break;
        
      case 1:
        blue();
        break;
        
      case 2:
        green();  
        break;
        
      case 3:
        red();  
        break;

      case 4:
        cylon();  
        break;

      case 5:
        fire();
        break;

     case 6:
        colorpalette();
        break;
        
      default:
        white();
        break;
  }

  delay(1000);
}

 void change_effect() 
{
  if ((millis() - last_change) > last_change_wait_short) 
  {
    last_change = millis();
    current_effect++;
    if (current_effect > 6) 
    {
      current_effect = 0;
    }
  }
}

void white()
{
   for( int i = 0; i < NUM_LEDS_2; i++) 
   {
    leds[i] = CRGB(220,220,50);
   }

   leds_1[0] = leds[0];
  
  FastLED.show();
}

void blue()
{
   for( int i = 0; i < NUM_LEDS_2; i++) 
   {
    leds[i] = CRGB(0,0,220);
   }

   leds_1[0] = CRGB(0,0,220);
  
  FastLED.show();
}

void green()
{
  leds_1[0] = CRGB (0,220,0);
  
   for( int i = 0; ((i < NUM_LEDS_2) && (current_effect == 2 )); i++) 
   {
    leds[i] = CRGB(0,220,0);
    FastLED.show();
   }
}

void red()
{
  leds_1[0] = CRGB (220,0,0);
    
  
  for(int i = (NUM_LEDS_2)-1; ((i >= 0) && (current_effect == 3 )); i--) 
  {
    leds[i] = CRGB(220,0,0);
    FastLED.show();
   }
}


void cylon()
{
  while(current_effect == 4)
  {
     static uint8_t hue = 0;
    // First slide the led in one direction
    for(int i = 0; ((i < NUM_LEDS_2) && (current_effect == 4 )); i++) 
    {
      leds[i] = CHSV(hue++, 220, 220);
      leds_1[0]=leds[i];
      FastLED.show(); 
      fadeall();
      delay(10);
    }
  
    // Now go in the other direction.  
    for(int i = (NUM_LEDS_2)-1; ((i >= 0) && (current_effect == 4 )); i--) 
    {
      // Set the i'th led to red 
      leds[i] = CHSV(hue++, 220, 220);
      leds_1[0]=leds[i];
      // Show the leds
      FastLED.show();
      fadeall();
      delay(10);
    }
  }
}


// Less cooling = taller flames.  More cooling = shorter flames.
#define COOLING  60

// Higher chance = more roaring fire.  Lower chance = more flickery fire.
#define SPARKING 110

void fire()
{
   while(current_effect == 5)
    {
// Array of temperature readings at each simulation cell
      static byte heat[NUM_LEDS_2];
  
    // Step 1.  Cool down every cell a little
      for( int i = 0; ((i < NUM_LEDS_2)&&(current_effect == 5)); i++) 
      {
        heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS_2) + 2));
      }
    
      // Step 2.  Heat from each cell drifts 'up' and diffuses a little
      for( int k= NUM_LEDS_2 - 1; k >= 2; k--) 
      {
        heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
      }

      FastLED.show(); // display this frame
      
      // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
      if( random8() < SPARKING ) 
      {
        int y = random8(7);
        heat[y] = qadd8( heat[y], random8(160,220) );
      }
  
      // Step 4.  Map from heat cells to LED colors
      for( int j = 0; ((j < NUM_LEDS_2)&&(current_effect == 5)); j++)
      {
        CRGB color = HeatColor( heat[j]);
        int pixelnumber;
        if( gReverseDirection ) {
          pixelnumber = (NUM_LEDS_2 -1) - j;
        } else {
          pixelnumber = j;
        }
        leds[pixelnumber] = color;
      }
      leds_1[0] = leds [125];
      FastLED.show(); // display this frame
      FastLED.delay(1000 / 60);
    }
}

//---Colorpalette-----------
void colorpalette()
{ 
  while(current_effect==6)
  {
    ChangePalettePeriodically();
    
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */
    
    FillLEDsFromPaletteColors( startIndex);
    leds_1[0]=leds[0];
    FastLED.show();
    FastLED.delay(1000 / 100);
  }
}

void FillLEDsFromPaletteColors( uint8_t colorIndex)
{
    uint8_t brightness = 220;
    
    for( int i = 0;(( i < NUM_LEDS_2)&&(current_effect==6)); i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
        colorIndex += 3;
    }
}


void ChangePalettePeriodically()
{
    uint8_t secondHand = (millis() / 1000) % 60;
    static uint8_t lastSecond = 99;
    
    if( lastSecond != secondHand) {
        lastSecond = secondHand;
        if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
        if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
        if( secondHand == 20)  { SetupPurpleAndGreenPalette();             currentBlending = LINEARBLEND; }
        if( secondHand == 25)  { SetupTotallyRandomPalette();              currentBlending = LINEARBLEND; }
        if( secondHand == 35)  { SetupBlackAndWhiteStripedPalette();       currentBlending = LINEARBLEND; }
        if( secondHand == 40)  { currentPalette = CloudColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
        if( secondHand == 55)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = LINEARBLEND; }
    }
}

// This function fills the palette with totally random colors.
void SetupTotallyRandomPalette()
{
    for( int i = 0; ((i < 16)&&(current_effect==6)); i++) {
        currentPalette[i] = CHSV( random8(), 220, random8());
    }
}

void SetupBlackAndWhiteStripedPalette()
{
    // 'black out' all 16 palette entries...
    fill_solid( currentPalette, 16, CRGB::Black);
    // and set every fourth one to white.
    currentPalette[0] = CRGB::White;
    currentPalette[4] = CRGB::White;
    currentPalette[8] = CRGB::White;
    currentPalette[12] = CRGB::White;
    
}

// This function sets up a palette of purple and green stripes.
void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 220, 220);
    CRGB green  = CHSV( HUE_GREEN, 220, 220);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}


const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray, // 'white' is too bright compared to red and blue
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};

