#include <FastLED.h>

#define LED_PIN_1   5
#define LED_PIN_2   9
#define LED_PIN_3   10
#define COLOR_ORDER1 GRB
#define COLOR_ORDER RGB
#define CHIPSET     WS2812B
#define NUM_LEDS_1  1
#define NUM_LEDS_2  150


#define BRIGHTNESS  200
#define FRAMES_PER_SECOND 60

bool gReverseDirection = false;

int current_effect = 0;
unsigned long last_change = 0;
#define last_change_wait 100

CRGB leds_1[NUM_LEDS_1];
CRGB leds_2[NUM_LEDS_2];

void setup() 
{
  FastLED.addLeds<CHIPSET, LED_PIN_1, COLOR_ORDER1>(leds_1, NUM_LEDS_1).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<CHIPSET, LED_PIN_2, COLOR_ORDER>(leds_2, NUM_LEDS_2).setCorrection( TypicalLEDStrip );
  FastLED.addLeds<CHIPSET, LED_PIN_3, COLOR_ORDER>(leds_2, NUM_LEDS_2).setCorrection( TypicalLEDStrip );
  FastLED.setBrightness( BRIGHTNESS );
  pinMode(7, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(7), change_effect, LOW);
}

void loop()
{
  // Add entropy to random number generator; we use a lot of it.
  // random16_add_entropy( random());

  //Fire2012(); // run simulation frame
  

 
  if (current_effect == 0) {
    leds_1[0] = CRGB(255,255,255);
  } else {
    leds_1[0] = CRGB(255,0,0);
  }

  for( int i = 0; i < NUM_LEDS_2; i++) {
    leds_2[i] = leds_1[0];
  }
  
  FastLED.show(); // display this frame
  //FastLED.delay(1000 / FRAMES_PER_SECOND);
  delay(1000);
}

int change_effect(int current) 
{
  if ((millis() - last_change) > last_change_wait) {
    last_change = millis();
    current_effect++;
    if (current_effect > 1) {
      current_effect = 0;
    }
  }
}
